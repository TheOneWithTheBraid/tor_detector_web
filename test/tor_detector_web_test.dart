import 'package:test/test.dart';

import 'package:tor_detector_web/tor_detector_web.dart';

/// all tests run in Firefox in our test setup
void main() {
  group('TOR browser test', () {
    test('Overall test', () async {
      final isTor = await TorBrowserDetector.isTorBrowser;
      expect(isTor, isFalse);
    });

    test('Firefox test', () async {
      expect(TorBrowserDetector.isFirefox, isTrue);
    });

    test('Plugins test', () async {
      expect(TorBrowserDetector.pluginsEmpty, isFalse);
    });

    test('Window size test', () async {
      expect(TorBrowserDetector.windowEqualsScreen, isFalse);
    });

    test('Timezone test', () async {
      expect(TorBrowserDetector.timeZoneIsUTC, isFalse);
    });

    test('Canvas test', () async {
      final canvasPassed = await TorBrowserDetector.canvasTest;
      expect(canvasPassed, isFalse);
    });
  });
}
