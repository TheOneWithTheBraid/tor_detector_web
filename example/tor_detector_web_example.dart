import 'dart:html';

import 'package:tor_detector_web/tor_detector_web.dart';

Future<void> main() async {
  final isTor = await TorBrowserDetector.isTorBrowser;
  window.console.log(isTor);
}
