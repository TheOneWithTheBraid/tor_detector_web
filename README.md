# tor_detector_web

*Identify TOR users and make you application ready for anonymous users.*

## Agreement and disclaimer

This plugin is meant to provide *additional* features to TOR users, not to ban them from content.

You may *not* use this package in any way to prevent people from using your application,
restricting features or any other regression.

As TOR browser in general tries to be undetectable, there is no guarantee that the present tests
still work in far future. Feel free to report issues about deprecated tests used in this package.

## Features

- overall TOR check
- user agent test
- `navigator.plugins` test
- window and viewport size test
- timezone offset test

## Usage

The `TorBrowserDetector` exposes some tests as well as a general getter to perform all tests.

```dart
Future<void> main() async {
  final isTor = await TorBrowserDetector.isTorBrowser;
  window.console.log(isTor);
}
```

## Contributing

All code should be properly formatted and the linter should not complain about anything:

```shell
dart format lib test example
dart run import_sorter:main --no-comments
dart analyze
```

All code should have basic tests. In order to have access to the `dart:html` library, ensure you
pass `-pfirefox` to the dart test command:

```shell
dart test -pfirefox # -pfirefox could be left out because of `dart_test.yaml`
```

## Additional information

Apart from the restriction above, this package is licensed under the terms and conditions of the
EUPL-1.2 as stated in [LICENSE](LICENSE).