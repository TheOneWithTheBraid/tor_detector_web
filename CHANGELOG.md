## 1.1.0

- deprecate canvas test
- add window and viewport size test
- add timezone offset test

## 1.0.0

- Initial version.
